package com.ideapoke.scheduler;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WishesTask {
	static Logger logger = LoggerFactory.getLogger(WishesTask.class);
	private String defaultservernameurl;
    private String defaultservername;
    static Set<String> maillist= new HashSet<String>();
   
  /* public static void main(String[] args) {
    	try
    	{
    	WishesTask wt = new WishesTask();
    	wt.printMe();
//    /	wt.fetchemailid();
    	}
    	catch(Exception ex)
    	{
    		logger.error("Exception in prepare sendTendmail"+ex);
    	}
    	
	}*/

    @SuppressWarnings("unused")
    public WishesTask(){
    
    	//IDEAPOKE
    	defaultservername = "https://www.ideapoke.com/";
        defaultservernameurl = "https://www.ideapoke.com";
            
        //IDEAPRIME
      
//        defaultservername = "https://www.ideaprime.com/";
//        defaultservernameurl = "https://www.ideaprime.com";
     
        
        //LOCAL SERVER
//      defaultservername = "https://ideaprime.com/";
//       defaultservernameurl = "https://ideaprime.com";
  
		
    }
	
	public void printMe()throws Exception 
	{
		sendTendmail();
	}

	public void sendTendmail() 
	{
		
		try
		{
    	logger.error("Inside111 sendmail task started1234444******************");
    	Emaillist lst =new Emaillist();
    	maillist = lst.emaillist();
		String mailcontent= preparemailcontent();
		logger.info("FINSIHED MAIL CONTENT");

		sendTrendMail(maillist,mailcontent);
		}
		catch(Exception ex)
		{
			logger.error("Exception in prepare sendTendmail"+ex);
		}
	}

	private void sendTrendMail(Set maillist, String mailcontent) 
	{
		String subject="Family, Food, Laughs and a Happy Diwali";
		String to[] = new String[1];
		String tempmailcontent = "";
		try{
			logger.info("STEP1 DiwaliWishes");
		if(maillist != null && maillist.size()>0){
			Iterator itr = maillist.iterator();
			while(itr.hasNext()){
				to[0] =(String) itr.next();
				
				tempmailcontent = mailcontent;
				tempmailcontent = tempmailcontent.replaceAll("<&emailid&>", to[0]);
				
				logger.error("EMAIL ID TO SEND THE MAIL----->"+to[0]);
				logger.error("content---->"+tempmailcontent);
				
				// send Trend Mail
				try{
				logger.info("------------STARTED TO SEND----------");	
				for(String t : to)
				{
					System.out.println(t);
				}
				(new SendMail()).sendMail(to, subject, tempmailcontent); 
				logger.info("------------SUCCESSFULLY SENT----------");	
				}catch (Exception e){
					logger.error("Exception  in sending the mailwish:",e);
				}  
			}
		}
		}catch(Exception e){
			logger.error("info in sendmail:"+e.toString());
		}
		
	}

	/*MAIL CONTENT*/
	private String preparemailcontent() 
	{
		String mailcontent="";
		try
		{
		
		String msgbody="";
		String emailid="";
		int count = 0;
		
		
		msgbody = "<html><head>"+
		"<title>Ideapoke - Diwali Wishes</title>"+
		"<!--"+
		""+
		"    An email present from your friends at Litmus (@litmusapp)"+
		""+
		"    Email is surprisingly hard. While this has been thoroughly tested, your mileage may vary."+
		"    It's highly recommended that you test using a service like Litmus (https://www.ideapoke.com) and your own devices."+
		""+
		"    Enjoy!"+
		""+
		" -->"+
		"<meta charset=\"utf-8\">"+
		"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"+
		"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"+
		"<style type=\"text/css\">"+
		"    /* CLIENT-SPECIFIC STYLES */"+
		"    body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */"+
		"    table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */"+
		"    img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */"+
		""+
		"    /* RESET STYLES */"+
		"    img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}"+
		"    table{border-collapse: collapse !important;}"+
		"    body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}"+
		""+
		"    /* iOS BLUE LINKS */"+
		"    a[x-apple-data-detectors] {"+
		"        color: inherit !important;"+
		"        text-decoration: none !important;"+
		"        font-size: inherit !important;"+
		"        font-family: inherit !important;"+
		"        font-weight: inherit !important;"+
		"        line-height: inherit !important;"+
		"    }"+
		""+
		"    /* MOBILE STYLES */"+
		"    @media screen and (max-width: 525px) {"+
		""+
		"        /* ALLOWS FOR FLUID TABLES */"+
		"        .wrapper {"+
		"          width: 100% !important;"+
		"        	max-width: 100% !important;"+
		"        }"+
		""+
		"        /* ADJUSTS LAYOUT OF LOGO IMAGE */"+
		"        .logo img {"+
		"          margin: 0 auto !important;"+
		"        }"+
		""+
		"        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */"+
		"        .mobile-hide {"+
		"          display: none !important;"+
		"        }"+
		""+
		"        .img-max {"+
		"          max-width: 100% !important;"+
		"          width: 100% !important;"+
		"          height: auto !important;"+
		"        }"+
		""+
		"        /* FULL-WIDTH TABLES */"+
		"        .responsive-table {"+
		"          width: 100% !important;"+
		"        }"+
		""+
		"        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */"+
		"        .padding {"+
		"          padding: 10px 5% 15px 5% !important;"+
		"        }"+
		""+
		"        .padding-meta {"+
		"          padding: 30px 5% 0px 5% !important;"+
		"          text-align: center;"+
		"        }"+
		""+
		"        .no-padding {"+
		"          padding: 0 !important;"+
		"        }"+
		""+
		"        .section-padding {"+
		"          padding: 50px 15px 50px 15px !important;"+
		"        }"+
		""+
		"        /* ADJUST BUTTONS ON MOBILE */"+
		"        .mobile-button-container {"+
		"            margin: 0 auto;"+
		"            width: 100% !important;"+
		"        }"+
		""+
		"        .mobile-button {"+
		"            padding: 15px !important;"+
		"            border: 0 !important;"+
		"            font-size: 16px !important;"+
		"            display: block !important;"+
		"        }"+
		""+
		"    }"+
		""+
		"    /* ANDROID CENTER FIX */"+
		"    div[style*=\"margin: 16px 0;\"] { margin: 0 !important; }"+
		"</style>"+
		"</head>"+
		"<body style=\"margin: 0 !important; padding: 0 !important;\">"+
		""+
		"<!-- HIDDEN PREHEADER TEXT -->"+
		"<div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">"+
		"    Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through..."+
		"</div>"+
		"";
		/*Header part starts*/
		msgbody = msgbody+"<!-- HEADER -->"+
		"<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width: 600px;font-family:arial, sans-serif;font-size:20px;border-right:1px solid #ccc;border-left:1px solid #ccc;border-top:1px solid #ccc;border-bottom:1px solid #ccc;\">"+
		"    <tbody><tr style=\"border-bottom:1px solid #ccc;\">"+
		"        <td bgcolor=\"#fff\" align=\"center\">"+
		"            <!--[if (gte mso 9)|(IE)]>"+
		"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">"+
		"            <tr>"+
		"            <td align=\"center\" valign=\"top\" width=\"500\">"+
		"            <![endif]-->";
		/*Header ends, bodypart starts*/
		msgbody = msgbody+"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" class=\"wrapper\">"+
		"                <tbody><tr>"+
		"                    <td align=\"center\" valign=\"top\" style=\"padding-left:7px; padding-right:7px;padding-top:10px;padding-bottom:10px;\" class=\"logo\">"+
		"<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
		"                          <tbody><tr>"+
		"                            <td width=\"38%\" align=\"left\" style=\"\"><a href=\"https://ideapoke.com\" target=\"_blank\"><img src=\"http://www.ideapoke.com/ipimages/webimages/default/homepage/logo.png\" border=\"0\" width=\"140\"></a></td>"+
		"                          </tr>"+
		"                    </tbody></table>"+
		"                    </td>"+
		"                </tr>"+
		"            </tbody></table>"+
		"            <!--[if (gte mso 9)|(IE)]>"+
		"            </td>"+
		"            </tr>"+
		"            </table>"+
		"            <![endif]-->"+
		"        </td>"+
		"    </tr>"+
		"    <tr>"+
		"        <td bgcolor=\"#ffffff\" align=\"center\" style=\"\" class=\"section-padding\">"+
		"            <!--[if (gte mso 9)|(IE)]>"+
		"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">"+
		"            <tr>"+
		"            <td align=\"center\" valign=\"top\" width=\"500\">"+
		"            <![endif]-->"+
		"            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" class=\"responsive-table\">"+
		"                <tbody><tr>"+
		"                    <td>"+
		"                        <!-- HERO IMAGE -->"+
		"                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
		"                            <tbody><tr>"+
		"                              	<td class=\"padding\" align=\"center\">"+
		"                                </td>"+
		"                            </tr>"+
		"                            <tr>"+
		"                                <td>"+
		"                                    <!-- COPY -->"+
		"                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
		"                                        <tbody><tr>"+
		"                                            <td align=\"left\" style=\"\" class=\"padding\"><img src=\"http://www.ideapoke.com/ipimages/mailimages/diwali2018.jpg\" style=\"vertical-align:top;\" width=\"600\" height=\"\" alt=\"\"></td>"+
		"                                        </tr>"+
		"                                    </tbody></table>"+
		"                                </td>"+
		"                            </tr>"+
		"							"+
		"                            <tr>"+
		"                                <td>"+
		"                                    <!-- COPY -->"+
		"                                    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"padding:30px;\">"+
		"                                                        <tbody>"+
		""+
		"                                                        <tr>"+
		"                                                            <td align=\"left\" style=\"text-align:center;padding-top:40px;padding-left:20px;padding-right:20px;font-size: 22px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #666666;\" class=\"padding\">Happy Diwali 2018</td>"+
		"                                                        </tr>"+
		"														<!--<tr>"+
		"                                                            <td align=\"left\" style=\"padding:5px; font-size: 13px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #aaaaaa;\" class=\"padding-meta\">Network Name</td>"+
		"                                                        </tr>-->"+
		"                                                        <tr>"+
		"                                                             <td align=\"left\" style=\"padding-top:5px;text-align:center;padding-left:20px;padding-right:20px; padding-bottom:20px;font-size: 14px; line-height: 24px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">Diwali is beautiful when family and friends come together to share happy times, <br> to share laughs, to enjoy food and to create wonderful memories… It is the celebration of life and of togetherness… Wishing you and your family a very Happy Diwali."+
		"                                                        </tr>"+
		""+
		"<tr>"+
		"									<td align=\"left\" style=\"padding: 10px 0 20px 20px;font-size: 14px; line-height: 20px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">Best Wishes,<br>Ideapoke</td>"+
		"									</tr>"+
		"                                                        <tr>"+
		"                                                            <td style=\"padding:0px;\" align=\"left\" class=\"padding\">"+
		"                                                                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"mobile-button-container\">"+
		"                                                                    <tbody><tr>"+
		"                                                                        <td align=\"center\">"+
		"                                                                            <!-- BULLETPROOF BUTTON -->"+
		"                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
		"                                                                                <tbody><tr>"+
		"                                                                                    <td align=\"left\">"+
		"                                                                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"mobile-button-container\">"+
		"                                                                                            <tbody>"+
		"                                                                                        </tbody></table>"+
		"                                                                                    </td>"+
		"                                                                                </tr>"+
		"                                                                            </tbody></table>"+
		"                                                                        </td>"+
		"                                                                    </tr>"+
		"                                                                </tbody></table>"+
		"                                                            </td>"+
		"                                                        </tr>"+
		""+
		"                                                    </tbody></table>"+
		"                                </td>"+
		"                            </tr>							"+
		"                            <tr>"+
		""+
		"                            </tr>"+
		"                        </tbody></table>"+
		"                    </td>"+
		"                </tr>"+
		"            </tbody></table>"+
		"            <!--[if (gte mso 9)|(IE)]>"+
		"            </td>"+
		"            </tr>"+
		"            </table>"+
		"            <![endif]-->"+
		"        </td>"+
		"    </tr>"+
		"   "+
		"   "+
		"   <tr>"+
		""+
		"    </tr>"+
		"  <tr>"+
		""+
		"    </tr>  ";
		/*Body ends,Footer part starts*/
		msgbody = msgbody +"    <tr>"+
		"        <td bgcolor=\"#333333\" align=\"center\" style=\"padding: 20px 0px;\">"+
		"            <!--[if (gte mso 9)|(IE)]>"+
		"            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">"+
		"            <tr>"+
		"            <td align=\"center\" valign=\"top\" width=\"500\">"+
		"            <![endif]-->"+
		"            <!-- footer -->"+
		"			<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 600px;\" class=\"responsive-table\">"+
		""+
		"                <tbody><tr>"+
		"                    <td align=\"center\" style=\"font-size: 14px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#fff;\">"+
		"                        Ideapoke, Where the world comes to innovate !  "+
		"                        <br>"+
		"                        Contact : <a href=\"mailto:info@ideapoke.com\" style=\"font-size: 12px;color: #57b4ff; text-decoration: none;\">info@ideapoke.com </a> "+
		"                    </td>"+
		""+
		"                </tr>"+
		"				"+
		"				                <tr>"+
		"                    <td align=\"center\" style=\"font-size: 14px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#ccc;\">"+
		"                        ----------------------------------------------------"+
		"                    </td>"+
		""+
		"                </tr>"+
		"				<tr>"+
		"									"+
		"					<td align=\"center\" style=\"padding-top:5px;font-style:italic;font-size: 11px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#fff;\">"+
		"					 India :+91 80 4372 3204 | US :+1 651 204 2720 "+
		"					</td>"+
		"				</tr>"+
		"				"+
		""+
		"							                <tr>"+
		"                    <td align=\"center\" style=\"padding-top:10px;font-style:italic;font-size: 14px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">"+
		"<a href=\"https://www.facebook.com/ideapoke\"><img src=\"https://www.ideapoke.com/ipimages/mailimages/fb.png\" style=\"\"></a><a href=\"https://twitter.com/ideapoke\"><img src=\"https://www.ideapoke.com/ipimages/mailimages/tw.png\" style=\"\"></a><a href=\"https://www.linkedin.com/company/ideapoke\"><img src=\"https://www.ideapoke.com/ipimages/mailimages/li.png\" style=\"\"></a><a href=\"https://plus.google.com/+Ideapoke/posts\"><img src=\"https://www.ideapoke.com/ipimages/mailimages/go.png\" style=\"\"></a><a href=\"https://www.youtube.com/channel/UCVUfidSj9xqPbex8_gjosMA\"><img src=\"https://www.ideapoke.com/ipimages/mailimages/yu.png\" style=\"\"></a><a href=\"http://blog.ideapoke.com/\"><img src=\"https://www.ideapoke.com/ipimages/mailimages/bl.png\" style=\"\"></a>"+
		"                    </td>"+
		"					"+
		"                </tr>"+
		"            </tbody></table>"+
		"            <!--[if (gte mso 9)|(IE)]>"+
		"            </td>"+
		"            </tr>"+
		"            </table>"+
		"            <![endif]-->"+
		"        </td>"+
		"    </tr>"+
		"</tbody></table>"+
		""+
		""+
		"</body></html>";

		mailcontent = msgbody;
		logger.info("PREPARE MAILCONTENT-->"+mailcontent);
//		System.out.println("PREPARE MAILCONTENT-->"+mailcontent);
		}
		catch(Exception ex){
			logger.error("Exception in prepare mailcontent"+ex);
		}
		return mailcontent;
	}

/*	private Set fetchemailid() {
		Set<String> mailidlist = new HashSet();
		try
		{
			String excelFilePath = "F://maillist.xlsx";
	        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
	         
	        Workbook workbook = new XSSFWorkbook(inputStream);
	        Sheet firstSheet = workbook.getSheetAt(0);
	        Iterator<Row> iterator = firstSheet.iterator();
	        String mailid = "";
	        while (iterator.hasNext()) {
	            Row nextRow = iterator.next();
	            Iterator<Cell> cellIterator = nextRow.cellIterator();
	             
	            while (cellIterator.hasNext()) {
	                Cell cell = cellIterator.next();
	                 
	                switch (cell.getCellType()) {
	                    case Cell.CELL_TYPE_STRING:
//	                    	System.out.println("mailid1--->"+mailid);
	                    	mailid = cell.getStringCellValue();
	                        break;
	                    case Cell.CELL_TYPE_BOOLEAN:
//	                    	System.out.println("mailid2--->"+mailid);
	                    	mailid = cell.getStringCellValue();
	                        break;
	                    case Cell.CELL_TYPE_NUMERIC:
//	                    	System.out.println("mailid3--->"+mailid);
	                    	mailid = cell.getStringCellValue();
	                        break;
	                   
	                }
	                if(mailid!=null && !mailid.equalsIgnoreCase("") && mailid.length() > 2)
	                {
	                	mailid = mailid.toLowerCase();
	                mailidlist.add(mailid);
	                }
	            }
		}
	        System.out.println(mailidlist.size());
	        System.out.println(1);
	        for(String email : mailidlist)
	        {
	        	logger.error("maillist.add(\""+email+"\");");
	        }
	        System.out.println(2);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return mailidlist;
		
	
	}*/
	
			
}
