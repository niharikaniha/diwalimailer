package com.ideapoke.scheduler;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
// Referenced classes of package com.ideapoke.scheduler:
//            EmailAuthenticator

public class SendMail{
	
	Logger logger = LoggerFactory.getLogger(SendMail.class);
    private boolean vision;
    private String strSmtp;
    private String strPort;
    Authenticator auth;

    public SendMail(){
    	//log = LogFactory.getLog(SendMail.class);
    }

    public void setStrSmtp(String strSmtp) {
        this.strSmtp = strSmtp;
    }

    public String getStrSmtp(){
        //strSmtp = "50.63.9.43";
    	strSmtp ="smtp.sendgrid.net";
    	// strSmtp ="smtp.mandrillapp.com";
        return strSmtp;
    }
    
    public String getStrSmtpPort(){
        //strPort = "25";
    	strPort = "587"; 
        return strPort;
    }

    @SuppressWarnings("unused")
	public void sendMail(String recipients[], String subject, String message)throws MessagingException {
        boolean debug = false;
        logger.error("auth "+auth);
        //auth = new EmailAuthenticator();
        if(auth == null){
        	logger.error("GETTING auth "+auth);
            auth = new EmailAuthenticator();
            logger.error("GOT auth "+auth);
        }
        String from = "Ideapoke";
        if(vision) {
            from = "TIFAC Team <Team@ideapoke.com>";
        }
        Properties props = new Properties();
        /*props.put("mail.smtp.host", getStrSmtp());
        props.put("mail.smtp.auth", "true");*/
        
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", getStrSmtp());
        props.put("mail.smtp.port",  getStrSmtpPort());
        props.put("mail.smtp.auth", "true");
        
        logger.error("SMTP "+getStrSmtp() +"   "+getStrSmtpPort());
        
        Session session = Session.getDefaultInstance(props, auth);
        session.setDebug(debug);
        MimeMessage contents = new MimeMessage(session);
        MimeMessage htmlMessage = new MimeMessage(session);
        Message msg = new MimeMessage(session);
        InternetAddress addressFrom = new InternetAddress(from);
        msg.setFrom(addressFrom);
        InternetAddress addressTo[] = new InternetAddress[recipients.length];
        for(int i = 0; i < recipients.length; i++)
        {
            addressTo[i] = new InternetAddress(recipients[i]);
        }

        msg.setRecipients(javax.mail.Message.RecipientType.TO, addressTo);
        message = message.replace("<&enter;>", "<br>");
        msg.setSubject(subject);
        msg.setText(message);
        msg.setContent(message, "text/html; charset=UTF-8");
        Transport.send(msg);
        //logger.info((new StringBuilder("mail sent to : ")).append(recipients[0]).toString());
    }

    @SuppressWarnings("unused")
	public void sendInviteMail(String from, String recipients[], String subject, String message)
        throws MessagingException
    {
        boolean debug = false;
        if(auth == null)
        {
            auth = new EmailAuthenticator();
        }
        Properties props = new Properties();
        /*props.put("mail.smtp.host", getStrSmtp());
        props.put("mail.smtp.auth", "true");*/
        
        
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", getStrSmtp());
        props.put("mail.smtp.port",  getStrSmtpPort());
        props.put("mail.smtp.auth", "true");
        
        
        
        Session session = Session.getDefaultInstance(props, auth);
        session.setDebug(debug);
        MimeMessage contents = new MimeMessage(session);
        MimeMessage htmlMessage = new MimeMessage(session);
        Message msg = new MimeMessage(session);
        InternetAddress addressFrom = new InternetAddress(from);
        msg.setFrom(addressFrom);
        InternetAddress addressTo[] = new InternetAddress[recipients.length];
        for(int i = 0; i < recipients.length; i++)
        {
            addressTo[i] = new InternetAddress(recipients[i]);
        }

        msg.setRecipients(javax.mail.Message.RecipientType.TO, addressTo);
        message = message.replace("<&enter;>", "<br>");
        msg.setSubject(subject);
        msg.setText(message);
        msg.setContent(message, "text/html; charset=UTF-8");
        Transport.send(msg);
    }

    public boolean isVision()
    {
        return vision;
    }

    public void setVision(boolean vision)
    {
        this.vision = vision;
    }
}
