package com.ideapoke.scheduler;

import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class WishesJob implements Job {

	@SuppressWarnings("rawtypes")
	public void execute(JobExecutionContext context)
        throws JobExecutionException
    {
        Map dataMap = context.getJobDetail().getJobDataMap();
        WishesTask task = (WishesTask)dataMap.get("WishesTask");
        try {
			task.printMe();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
